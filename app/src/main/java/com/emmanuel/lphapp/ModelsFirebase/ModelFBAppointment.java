package com.emmanuel.lphapp.ModelsFirebase;

import com.emmanuel.lphapp.Misc.AppConstants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Firebase Model data class for Appointment Object.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 24-Jan-18,
 * at 9:42AM.
 */
@IgnoreExtraProperties
public class ModelFBAppointment {

    private String patient_name;
    private String contact_number;
    private String hospital_name;
    private String preferred_date;
    private String preferred_time;
    private String specialist;
    private String current_condition;


    public ModelFBAppointment() {}


    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getPreferred_date() {
        return preferred_date;
    }

    public void setPreferred_date(String preferred_date) {
        this.preferred_date = preferred_date;
    }

    public String getPreferred_time() {
        return preferred_time;
    }

    public void setPreferred_time(String preferred_time) {
        this.preferred_time = preferred_time;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getCurrent_condition() {
        return current_condition;
    }

    public void setCurrent_condition(String current_condition) {
        this.current_condition = current_condition;
    }

    @Exclude
    public HashMap<String, Object> codeToGenerateAppointmentHashMap() {

        HashMap<String, Object> mapFirebaseAppointment = new HashMap<>();
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_PATIENT_NAME, this.getPatient_name());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_CONTACT_NUMBER, this.getContact_number());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_HOSPITAL_NAME, this.getHospital_name());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_PREFERRED_DATE, this.getPreferred_date());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_PREFERRED_TIME, this.getPreferred_time());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_SPECIALIST, this.getSpecialist());
        mapFirebaseAppointment.put(AppConstants.sDBFIELD_CURRENT_CONDITION, this.getCurrent_condition());

        return mapFirebaseAppointment;
    }

}
