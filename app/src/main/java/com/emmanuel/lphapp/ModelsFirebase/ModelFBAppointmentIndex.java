package com.emmanuel.lphapp.ModelsFirebase;

import com.emmanuel.lphapp.Misc.AppConstants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Firebase Data Model class for an Appointment Index object.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 24-Jan-18,
 * at 10:15AM.
 */
@IgnoreExtraProperties
public class ModelFBAppointmentIndex {

    private String preferred_date;
    private String preferred_time;


    public ModelFBAppointmentIndex() {}


    public String getPreferred_date() {
        return preferred_date;
    }

    public void setPreferred_date(String preferred_date) {
        this.preferred_date = preferred_date;
    }

    public String getPreferred_time() {
        return preferred_time;
    }

    public void setPreferred_time(String preferred_time) {
        this.preferred_time = preferred_time;
    }

    @Exclude
    public HashMap<String, Object> codeToGenerateAppointmentIndexHashMap() {

        HashMap<String, Object> mapAppointmentIndex = new HashMap<>();
        mapAppointmentIndex.put(AppConstants.sDBFIELD_PREFERRED_DATE, this.getPreferred_date());
        mapAppointmentIndex.put(AppConstants.sDBFIELD_PREFERRED_TIME, this.getPreferred_time());

        return mapAppointmentIndex;
    }
}
