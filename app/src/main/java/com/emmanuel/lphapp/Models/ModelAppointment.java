package com.emmanuel.lphapp.Models;

/**
 * Data Model class for Appointments.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 23-Jan-18,
 * at 12:20PM.
 */
public class ModelAppointment {

    private String sAppointmentId;
    private String sPatientName;
    private String sContactNumber;
    private String sHospitalName;
    private String sPreferredDate;
    private String sPreferredTime;
    private String sSpecialist;
    private String sCurrentCondition;

    public String getsAppointmentId() {
        return sAppointmentId;
    }

    public void setsAppointmentId(String sAppointmentId) {
        this.sAppointmentId = sAppointmentId;
    }

    public String getsPatientName() {
        return sPatientName;
    }

    public void setsPatientName(String sPatientName) {
        this.sPatientName = sPatientName;
    }

    public String getsContactNumber() {
        return sContactNumber;
    }

    public void setsContactNumber(String sContactNumber) {
        this.sContactNumber = sContactNumber;
    }

    public String getsHospitalName() {
        return sHospitalName;
    }

    public void setsHospitalName(String sHospitalName) {
        this.sHospitalName = sHospitalName;
    }

    public String getsPreferredDate() {
        return sPreferredDate;
    }

    public void setsPreferredDate(String sPreferredDate) {
        this.sPreferredDate = sPreferredDate;
    }

    public String getsPreferredTime() {
        return sPreferredTime;
    }

    public void setsPreferredTime(String sPreferredTime) {
        this.sPreferredTime = sPreferredTime;
    }

    public String getsSpecialist() {
        return sSpecialist;
    }

    public void setsSpecialist(String sSpecialist) {
        this.sSpecialist = sSpecialist;
    }

    public String getsCurrentCondition() {
        return sCurrentCondition;
    }

    public void setsCurrentCondition(String sCurrentCondition) {
        this.sCurrentCondition = sCurrentCondition;
    }
}
