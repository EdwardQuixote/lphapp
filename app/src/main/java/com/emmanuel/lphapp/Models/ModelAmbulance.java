package com.emmanuel.lphapp.Models;

/**
 * Data Model class for Ambulance.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 23-Jan-18,
 * at 12:20PM.
 */
public class ModelAmbulance {

    private String sAmbulanceServiceName;
    private String sAmbulanceServiceContactNumber;

    public String getsAmbulanceServiceContactNumber() {
        return sAmbulanceServiceContactNumber;
    }

    public void setsAmbulanceServiceContactNumber(String sAmbulanceServiceContactNumber) {
        this.sAmbulanceServiceContactNumber = sAmbulanceServiceContactNumber;
    }

    public String getsAmbulanceServiceName() {
        return sAmbulanceServiceName;
    }

    public void setsAmbulanceServiceName(String sAmbulanceServiceName) {
        this.sAmbulanceServiceName = sAmbulanceServiceName;
    }
}
