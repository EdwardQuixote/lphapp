package com.emmanuel.lphapp.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.emmanuel.lphapp.Misc.AppConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * AsyncTask class for DB Operations on BookAppointment.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 24-Jan-18,
 * at 4:42PM.
 */
public class DBWorker extends AsyncTask<String, Void, String> {

    private Context coxContext;

    private ProgressDialog pdgProgressDialog;

    public DBWorker(Context context) {
        this.coxContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pdgProgressDialog = new ProgressDialog(coxContext);
        pdgProgressDialog.setMessage("Loading. . . .");
        pdgProgressDialog.setCanceledOnTouchOutside(false);
        pdgProgressDialog.show();

    }

    @Override
    protected String doInBackground(String... strings) {

        String sExecute_Tag = strings[0];
        if (sExecute_Tag.equals(AppConstants.sEXECUTETAG_BOOK_APPOINTMENT)) {
            String sPatientName = strings[1];
            String sContactNumber = strings[2];
            String sHospitalName = strings[3];
            String sPreferredDate = strings[4];
            String sPreferredTime = strings[5];
            String sSpecialist = strings[6];
            String sCurrentCondition = strings[7];

            try {
                String sEncodedURLParams =
                        URLEncoder.encode(AppConstants.sCOL_PATIENT_NAME, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sPatientName, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_CONTACT_NUMBER, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sContactNumber, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_HOSPITAL_NAME, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sHospitalName, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_PREFERRED_DATE, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sPreferredDate, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_PREFERRED_TIME, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sPreferredTime, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_SPECIALIST, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sSpecialist, AppConstants.sENCODING_SCHEME) +
                                "&" +
                                URLEncoder.encode(AppConstants.sCOL_CURRENT_CONDITION, AppConstants.sENCODING_SCHEME) + "=" +
                                URLEncoder.encode(sCurrentCondition, AppConstants.sENCODING_SCHEME);

                Log.e("DBWorker", "doInBackground() - sEncodedURLParams " + sEncodedURLParams);     //  TODO: For Testing ONLY

                //  Get & Open connection to DB Server.
                URL urlDBServerURL = new URL(AppConstants.sBOOK_APPOINTMENT_URL);
                HttpURLConnection hucDBServerConnection = (HttpURLConnection) urlDBServerURL.openConnection();
                hucDBServerConnection.setRequestMethod("POST");
                hucDBServerConnection.setDoInput(true);

                //  Write Params to Server.
                OutputStreamWriter oswOutputStreamWriter = new OutputStreamWriter(hucDBServerConnection.getOutputStream());
                oswOutputStreamWriter.write(sEncodedURLParams);
                oswOutputStreamWriter.flush();
                oswOutputStreamWriter.close();

                Log.e("DBWorker", "doInBackground() - Connection Established! Response Code: " + hucDBServerConnection.getResponseCode());     //  TODO: For Testing ONLY
                Log.e("DBWorker", "doInBackground() - Output written using OutputStreamWriter!");     //  TODO: For Testing ONLY

                BufferedReader bfrServerResponseReader = new BufferedReader(new InputStreamReader(hucDBServerConnection.getInputStream()));
                StringBuilder sbResponse = new StringBuilder();
                String sResponseLine = null;
                while ((sResponseLine = bfrServerResponseReader.readLine()) != null) {
                    sbResponse.append(sResponseLine);
                }
                String sServerResponse = sbResponse.toString();

                Log.e("DBWorker", "doInBackground() - sServerResponseCode: " + hucDBServerConnection.getResponseCode() + "    sServerResponse: " + sServerResponse);

                return sServerResponse;
            } catch (UnsupportedEncodingException ueex) {
                Log.e("DBWorker", "doInBackground() - UnsupportedEncodingException (uuex): " + ueex.toString());
            } catch (MalformedURLException muex) {
                Log.e("DBWorker", "doInBackground() - MalformedURLException (muex): " + muex.toString());
            } catch (IOException ioex) {
                Log.e("DBWorker", "doInBackground() - IOException (ioex): " + ioex.toString());
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        pdgProgressDialog.dismiss();

        AlertDialog.Builder adgbldResponseDialog = new AlertDialog.Builder(coxContext);
        adgbldResponseDialog.setTitle("Server Response");
        adgbldResponseDialog.setMessage(result);

        AlertDialog adgServerResponse = adgbldResponseDialog.create();
        adgServerResponse.show();

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

}
