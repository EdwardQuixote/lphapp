package com.emmanuel.lphapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.emmanuel.lphapp.Models.ModelAmbulance;
import com.emmanuel.lphapp.R;

import java.util.ArrayList;

/**
 * Adapter class for Ambulance ListView.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 23-Jan-18,
 * at 12:19PM.
 */
public class AdapterAmbulances extends BaseAdapter {

    private ArrayList<ModelAmbulance> arylAmbulances;

    public AdapterAmbulances(ArrayList<ModelAmbulance> arrayListAmbulances) {
        this.arylAmbulances = arrayListAmbulances;
    }

    @Override
    public int getCount() {
        return arylAmbulances.size();
    }

    @Override
    public Object getItem(int position) {
        return arylAmbulances.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LSTAmbulanceViewHolder clsViewHolder = null;
        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_lst_ambulances, parent, false);

            clsViewHolder = new LSTAmbulanceViewHolder(convertView);

            convertView.setTag(clsViewHolder);
        } else {

            clsViewHolder = (LSTAmbulanceViewHolder) convertView.getTag();
        }

        clsViewHolder.txtName.setText(arylAmbulances.get(position).getsAmbulanceServiceName());
        clsViewHolder.txtContactNumber.setText(arylAmbulances.get(position).getsAmbulanceServiceContactNumber());

        return convertView;
    }


    private class LSTAmbulanceViewHolder {

        private TextView txtName;
        private TextView txtContactNumber;

        public LSTAmbulanceViewHolder(View itemView) {

            txtName = (TextView) itemView.findViewById(R.id.txtAmbulancesName);
            txtContactNumber = (TextView) itemView.findViewById(R.id.txtAmbulancesContactNumber);

        }
    }
}
