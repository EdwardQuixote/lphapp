package com.emmanuel.lphapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.emmanuel.lphapp.Adapters.AdapterRVViewAppointments;
import com.emmanuel.lphapp.Misc.AppConstants;
import com.emmanuel.lphapp.Models.ModelAppointment;
import com.emmanuel.lphapp.ModelsFirebase.ModelFBAppointment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ViewAppointmentsActivity extends AppCompatActivity implements AdapterRVViewAppointments.InterfaceAdapViewAppointments {

    private RecyclerView rvAppointments;

    private ProgressDialog pdgProgressDialog;

    private FirebaseDatabase fbdbFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appointments);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize
     * class variables,
     * and UI Objects.
     *
     * Called in (Override)this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        pdgProgressDialog = new ProgressDialog(ViewAppointmentsActivity.this);
        pdgProgressDialog.setMessage("Loading. . . .");
        pdgProgressDialog.setCanceledOnTouchOutside(false);

        fbdbFirebaseDatabase = FirebaseDatabase.getInstance();

        LinearLayoutManager llmLayoutManager = new LinearLayoutManager(ViewAppointmentsActivity.this);

        rvAppointments = (RecyclerView) this.findViewById(R.id.rvViewAppointments);
        rvAppointments.setLayoutManager(llmLayoutManager);
        rvAppointments.setItemAnimator(new DefaultItemAnimator());
        rvAppointments.setHasFixedSize(false);

        codeToFetchAppointmentsFromFirebase();

    }

    /**
     * TODO: JavaDOC.
     * @return
     */
    private void codeToFetchAppointmentsFromFirebase() {

        pdgProgressDialog.show();

        DatabaseReference dbrefAppointments = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENTS);
        dbrefAppointments.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onDataChange() - dataSnapshot: " + dataSnapshot);

                if (dataSnapshot.getValue() != null) {

                    ArrayList<ModelAppointment> arylAppointments = new ArrayList<>();

                    for (DataSnapshot dsAppointmentDataSnapshot : dataSnapshot.getChildren()) {

                        String sAppointmentID = dsAppointmentDataSnapshot.getKey();

                        ModelFBAppointment clsModelFBAppointment = dsAppointmentDataSnapshot.getValue(ModelFBAppointment.class);
                        if (clsModelFBAppointment != null) {

                            String sPatientName = clsModelFBAppointment.getPatient_name();
                            String sContactNumber = clsModelFBAppointment.getContact_number();
                            String sHospitalName = clsModelFBAppointment.getHospital_name();
                            String sPreferredDate = clsModelFBAppointment.getPreferred_date();
                            String sPreferredTime = clsModelFBAppointment.getPreferred_time();
                            String sSpecialist = clsModelFBAppointment.getSpecialist();
                            String sCurrentConditions = clsModelFBAppointment.getCurrent_condition();

                            ModelAppointment clsModelAppointment = new ModelAppointment();
                            clsModelAppointment.setsAppointmentId(sAppointmentID);
                            clsModelAppointment.setsPatientName(sPatientName);
                            clsModelAppointment.setsContactNumber(sContactNumber);
                            clsModelAppointment.setsHospitalName(sHospitalName);
                            clsModelAppointment.setsPreferredDate(sPreferredDate);
                            clsModelAppointment.setsPreferredTime(sPreferredTime);
                            clsModelAppointment.setsSpecialist(sSpecialist);
                            clsModelAppointment.setsCurrentCondition(sCurrentConditions);

                            arylAppointments.add(clsModelAppointment);

                        }
                    }

                    rvAppointments.setAdapter(new AdapterRVViewAppointments(ViewAppointmentsActivity.this, arylAppointments));

                    pdgProgressDialog.dismiss();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onCancelled() - databaseError: " + databaseError);

                pdgProgressDialog.dismiss();
            }
        });

//        dbrefAppointments.addChildEventListener(new ChildEventListener() {
//
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//
//                Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onChildAdded() - dataSnapshot: " + dataSnapshot + "   s: " + s);
//
//                if (dataSnapshot != null) {
//
//                    String sAppointmentID = dataSnapshot.getKey();
//
//                    ModelFBAppointment clsModelFBAppointment = dataSnapshot.getValue(ModelFBAppointment.class);
//                    if (clsModelFBAppointment != null) {
//
//                        String sPatientName = clsModelFBAppointment.getPatient_name();
//                        String sContactNumber = clsModelFBAppointment.getContact_number();
//                        String sHospitalName = clsModelFBAppointment.getHospital_name();
//                        String sPreferredDate = clsModelFBAppointment.getPreferred_date();
//                        String sPreferredTime = clsModelFBAppointment.getPreferred_time();
//                        String sSpecialist = clsModelFBAppointment.getSpecialist();
//                        String sCurrentConditions = clsModelFBAppointment.getCurrent_condition();
//
//                        ModelAppointment clsModelAppointment = new ModelAppointment();
//                        clsModelAppointment.setsAppointmentId(sAppointmentID);
//                        clsModelAppointment.setsPatientName(sPatientName);
//                        clsModelAppointment.setsContactNumber(sContactNumber);
//                        clsModelAppointment.setsHospitalName(sHospitalName);
//                        clsModelAppointment.setsPreferredDate(sPreferredDate);
//                        clsModelAppointment.setsPreferredTime(sPreferredTime);
//                        clsModelAppointment.setsSpecialist(sSpecialist);
//                        clsModelAppointment.setsCurrentCondition(sCurrentConditions);
//
//                        arylAppointments.add(clsModelAppointment);
//
//                        if (rvAppointments.getAdapter() != null) {
//                            Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onChildAdded() - Adapter ISN'T NULL! arylAppointments.size(): " + arylAppointments.size());
//
//                            rvAppointments.getAdapter().notifyDataSetChanged();
//                        }
//
//                    }
//                }
//
//                if (pdgProgressDialog.isShowing())
//                    pdgProgressDialog.dismiss();
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//                Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onChildRemoved() - dataSnapshot: " + dataSnapshot);
//
//                if (dataSnapshot != null) {
//
//                    String sAppointmentID = dataSnapshot.getKey();
//
//                    ModelFBAppointment clsModelFBAppointment = dataSnapshot.getValue(ModelFBAppointment.class);
//                    if (clsModelFBAppointment != null) {
//
//                        String sPatientName = clsModelFBAppointment.getPatient_name();
//                        String sContactNumber = clsModelFBAppointment.getContact_number();
//                        String sHospitalName = clsModelFBAppointment.getHospital_name();
//                        String sPreferredDate = clsModelFBAppointment.getPreferred_date();
//                        String sPreferredTime = clsModelFBAppointment.getPreferred_time();
//                        String sSpecialist = clsModelFBAppointment.getSpecialist();
//                        String sCurrentConditions = clsModelFBAppointment.getCurrent_condition();
//
//                        ModelAppointment clsModelAppointment = new ModelAppointment();
//                        clsModelAppointment.setsAppointmentId(sAppointmentID);
//                        clsModelAppointment.setsPatientName(sPatientName);
//                        clsModelAppointment.setsContactNumber(sContactNumber);
//                        clsModelAppointment.setsHospitalName(sHospitalName);
//                        clsModelAppointment.setsPreferredDate(sPreferredDate);
//                        clsModelAppointment.setsPreferredTime(sPreferredTime);
//                        clsModelAppointment.setsSpecialist(sSpecialist);
//                        clsModelAppointment.setsCurrentCondition(sCurrentConditions);
//
//                        arylAppointments.remove(clsModelAppointment);
//
//                        if (rvAppointments.getAdapter() != null) {
//                            Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onChildRemoved() - Adapter ISN'T NULL! arylAppointments.size(): " + arylAppointments.size());
//
//                            rvAppointments.getAdapter().notifyDataSetChanged();
//                        }
//
//                    }
//                }
//
//                if (pdgProgressDialog.isShowing())
//                    pdgProgressDialog.dismiss();
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.e("ViewAppointments", "codeToFetchAppointmentsFromFirebase() - dbrefAppointments - onCancelled() - databaseError: " + databaseError);
//
//                pdgProgressDialog.dismiss();
//            }
//
//        });

    }

    /**
     * TODO: JavaDoc
     *
     * @param appointmentId
     */
    private void codeToDeleteAppointmentFromFirebase(final String appointmentId) {

        pdgProgressDialog.show();

        DatabaseReference dbrefAppointments = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENTS);
        dbrefAppointments.child(appointmentId).removeValue(new DatabaseReference.CompletionListener() {

            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                if (databaseError != null) {
                    Log.e("ViewAppointments", "codeToDeleteAppointmentFromFirebase() - dbrefAppointments - onComplete() - databaseError: " + databaseError);

                    Snackbar.make(rvAppointments, "Appointment Not Cancelled Successfully!", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(rvAppointments, "Appointment Cancelled Successfully!", Snackbar.LENGTH_LONG).show();

                    DatabaseReference dbrefAppointmentIndex = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENT_INDEX);
                    dbrefAppointmentIndex.child(appointmentId).removeValue(new DatabaseReference.CompletionListener() {

                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if (databaseError != null) {
                                Log.e("ViewAppointments", "codeToDeleteAppointmentFromFirebase() - dbrefAppointmentIndex - onComplete() - databaseError: " + databaseError);
                            }

                            pdgProgressDialog.dismiss();

                        }
                    });
                }
            }

        });

    }


    @Override
    public void codeToDeleteSelectedAppointmentRecord(String appointmentId) {

        Log.e("ViewAppointments", "codeToDeleteSelectedAppointemntRecord() -- appointmentId: " + appointmentId);

        codeToDeleteAppointmentFromFirebase(appointmentId);

    }
}
