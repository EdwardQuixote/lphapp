package com.emmanuel.lphapp.Misc;

/**
 * Class taht holds static Constants used in this Project.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 23-Jan-18,
 * at 12:17PM.
 */
public class AppConstants {

    //      General Strings
    public static final String sENCODING_SCHEME = "UTF-8";
    public static final String sBOOK_APPOINTMENT_URL = "http://192.168.8.102/my_php/Manu_LPHApp/book_appointment.php";
    public static final String sVIEW_APPOINTMENT_URL = "http://localhost:3306/LPHApp/view_appointment.php";


    //      Tags:
    public static final String sEXECUTETAG_BOOK_APPOINTMENT = "com.emmanuel.lphapp.BOOK_APPOINTMENT";


    //      Appointment Column Names:
    public static final String sCOL_PATIENT_NAME = "patient_name";
    public static final String sCOL_CONTACT_NUMBER = "contact_number";
    public static final String sCOL_HOSPITAL_NAME = "hospital_name";
    public static final String sCOL_PREFERRED_DATE = "preferred_date";
    public static final String sCOL_PREFERRED_TIME = "preferred_time";
    public static final String sCOL_SPECIALIST = "specialist";
    public static final String sCOL_CURRENT_CONDITION = "current_condition";


    //      Firebase Constants:
    public static final String sDBREF_APPOINTMENTS = "appointments";
    public static final String sDBREF_APPOINTMENT_INDEX = "appointment_index";
    public static final String sDBFIELD_PATIENT_NAME = "patient_name";
    public static final String sDBFIELD_CONTACT_NUMBER = "contact_number";
    public static final String sDBFIELD_HOSPITAL_NAME = "hospital_name";
    public static final String sDBFIELD_PREFERRED_DATE = "preferred_date";
    public static final String sDBFIELD_PREFERRED_TIME = "preferred_time";
    public static final String sDBFIELD_SPECIALIST = "specialist";
    public static final String sDBFIELD_CURRENT_CONDITION = "current_condition";

}
