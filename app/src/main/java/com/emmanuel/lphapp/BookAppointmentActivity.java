package com.emmanuel.lphapp;

import android.app.ProgressDialog;
import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.emmanuel.lphapp.InternalDB.DBHelper;
import com.emmanuel.lphapp.Misc.AppConstants;
import com.emmanuel.lphapp.ModelsFirebase.ModelFBAppointment;
import com.emmanuel.lphapp.ModelsFirebase.ModelFBAppointmentIndex;
import com.emmanuel.lphapp.Utils.DBWorker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

public class BookAppointmentActivity extends AppCompatActivity {

    private TextInputLayout tilPatientName;
    private TextInputLayout tilContactNumber;
    private TextInputLayout tilCondition;

    private TextInputEditText tiedPatientName;
    private TextInputEditText tiedContactNumber;
    private TextInputEditText tiedCurrentCondition;

    private Spinner spnHospitalName;
    private Spinner spnSpecialist;

    private DatePicker dpPreferredDate;
    private TimePicker tpPreferredTime;

    private ProgressDialog pdgProgressDialog;

    private FirebaseDatabase fbdbFirebaseDatabase;

    private String sPreferredDate;
    private String sPreferredTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects.
     * <p>
     * Called in (Override)this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        String[] saryHospitals = {
                "Borana Dispensary",
                "Mpalla Dispensary",
                "Oljogi Dispensary",
                "Nyahururu Private Hospital",
                "Good Hope Hospital",
                "Oljabet Annex and Nursing Home",
                "Maryhill Medical Clinic",
                "Segera Mission Dispensary",
                "Nanyuki Cottage Hospital"
        };
        String[] sarySpecialists = {
                "Dentist",
                "Surgeon",
                "Bone Doctor",
                "Optician",
                "Physician",
                "Post-trauma Specialist",
                "Dermatologist",
                "Neurosurgeon"
        };

        ArrayAdapter<String> adpHospitals = new ArrayAdapter<>(BookAppointmentActivity.this, android.R.layout.simple_dropdown_item_1line, saryHospitals);
        ArrayAdapter<String> adpSpecialists = new ArrayAdapter<>(BookAppointmentActivity.this, android.R.layout.simple_spinner_dropdown_item, sarySpecialists);

        pdgProgressDialog = new ProgressDialog(BookAppointmentActivity.this);
        pdgProgressDialog.setMessage("Loading. . . .");
        pdgProgressDialog.setCanceledOnTouchOutside(false);

        fbdbFirebaseDatabase = FirebaseDatabase.getInstance();

        Calendar calCalendar = Calendar.getInstance();
        int iYear = calCalendar.get(Calendar.YEAR);
        int iMonthOfYear = calCalendar.get(Calendar.MONTH);
        int iDayOfMonth = calCalendar.get(Calendar.DAY_OF_MONTH);

        tilPatientName = (TextInputLayout) this.findViewById(R.id.tilBAPatientName);
        tilContactNumber = (TextInputLayout) this.findViewById(R.id.tilBAContactNumber);
        tilCondition = (TextInputLayout) this.findViewById(R.id.tilBACurrentCondition);

        tiedPatientName = (TextInputEditText) this.findViewById(R.id.tiedBAPatientName);
        tiedContactNumber = (TextInputEditText) this.findViewById(R.id.tiedBAContactNumber);
        tiedCurrentCondition = (TextInputEditText) this.findViewById(R.id.tiedBACurrentCondition);

        spnHospitalName = (Spinner) this.findViewById(R.id.spnBAHospitalName);
        spnSpecialist = (Spinner) this.findViewById(R.id.spnBASpecialist);
        spnHospitalName.setAdapter(adpHospitals);
        spnSpecialist.setAdapter(adpSpecialists);

        dpPreferredDate = (DatePicker) this.findViewById(R.id.dpBAPreferredDate);
        tpPreferredTime = (TimePicker) this.findViewById(R.id.tpBAPreferredTime);
        dpPreferredDate.init(iYear, iMonthOfYear, iDayOfMonth, odclPreferredDate);
        tpPreferredTime.setOnTimeChangedListener(otclPreferredTime);

        Button btnBook = (Button) this.findViewById(R.id.btnBABookAppointment);
        btnBook.setOnClickListener(clkBookAppointment);

    }

    /**
     * Method to validate if the data,
     * the user provided is valid.
     * <p>
     * Called in this.clkBookAppointment.onClick();
     *
     * @return (boolean)
     */
    private boolean codeToValidateUserInput() {

        if (tiedPatientName.getText().toString().equalsIgnoreCase("")) {
            tilPatientName.setError("Please Provide the Patient's Name.");

            tiedPatientName.requestFocus();

            return false;
        } else if (tiedContactNumber.getText().toString().equalsIgnoreCase("")) {
            tilContactNumber.setError("Please Provide the Patient's Contact Number.");

            tiedContactNumber.requestFocus();

            return false;
        } else if (tiedCurrentCondition.getText().toString().equalsIgnoreCase("")) {
            tilCondition.setError("Please Provide the Patient's Health Condition.");

            tiedCurrentCondition.requestFocus();

            return false;
        } else {

            tilPatientName.setError("");
            tilContactNumber.setError("");
            tilCondition.setError("");

            return true;
        }
    }

    /**
     * TODO: JavaDoc.
     */
    private void codeToCheckCollusionOfAppointments() {

        pdgProgressDialog.show();

        final String sPreferredDate = this.sPreferredDate;
        final String sPreferredTime = this.sPreferredTime;

        DatabaseReference dbrefAppointmentIndex = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENT_INDEX);
        dbrefAppointmentIndex.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.e("BookAppointment", "codeToCheckCollusionOfAppointments() - dbrefAppointmentIndex -  onDataChange() - dataSnapshot: " + dataSnapshot);

                if (dataSnapshot != null) {

                    boolean boolIsValidAppointment = true;

                    for (DataSnapshot dsAppointmentIndex : dataSnapshot.getChildren()) {

                        ModelFBAppointmentIndex clsModelFBAppointmentIndex = dsAppointmentIndex.getValue(ModelFBAppointmentIndex.class);
                        if (clsModelFBAppointmentIndex != null) {
                            String sIndexedPreferredDate = clsModelFBAppointmentIndex.getPreferred_date();
                            String sIndexedPreferredTime = clsModelFBAppointmentIndex.getPreferred_time();

                            if (sPreferredDate.equalsIgnoreCase(sIndexedPreferredDate)) {

                                if (sPreferredTime.equalsIgnoreCase(sIndexedPreferredTime)) {

                                    Snackbar.make(tiedCurrentCondition, "You already have an appointment on this Date at this Time.", Snackbar.LENGTH_LONG).show();

                                    boolIsValidAppointment = false;

                                    Log.e("BookAppointment", "codeToCheckCollusionOfAppointments() - dbrefAppointmentIndex -  onDataChange() - MATCHING DATE AND TIME! boolIsValidAppointment: " + boolIsValidAppointment);

                                    break;
                                }
                            }
                        }
                    }

                    if (boolIsValidAppointment) {
                        Log.e("BookAppointment", "codeToCheckCollusionOfAppointments() - dbrefAppointmentIndex -  onDataChange() - boolIsValidAppointment: " + boolIsValidAppointment);

                        codeToBookAppointmentOnFirebase();
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("BookAppointment", "codeToCheckCollusionOfAppointments() - dbrefAppointmentIndex -  onCancelled() - databaseError: " + databaseError);

                pdgProgressDialog.dismiss();
            }

        });

    }

    /**
     * TODO: JavaDOC.
     */
    private void codeToBookAppointmentOnFirebase() {

        String sPatientName = tiedPatientName.getText().toString();
        String sContactNumber = tiedContactNumber.getText().toString();
        String sHospitalName = spnHospitalName.getSelectedItem().toString();
        final String sPreferredDate = this.sPreferredDate;
        final String sPreferredTime = this.sPreferredTime;
        String sSpecialist = spnSpecialist.getSelectedItem().toString();
        String sCurrentCondition = tiedCurrentCondition.getText().toString();

        ModelFBAppointment clsModelFBAppointment = new ModelFBAppointment();
        clsModelFBAppointment.setPatient_name(sPatientName);
        clsModelFBAppointment.setContact_number(sContactNumber);
        clsModelFBAppointment.setHospital_name(sHospitalName);
        clsModelFBAppointment.setPreferred_date(sPreferredDate);
        clsModelFBAppointment.setPreferred_time(sPreferredTime);
        clsModelFBAppointment.setSpecialist(sSpecialist);
        clsModelFBAppointment.setCurrent_condition(sCurrentCondition);

        HashMap<String, Object> mapAppointmentDetails = clsModelFBAppointment.codeToGenerateAppointmentHashMap();

        DatabaseReference dbrefAppointments = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENTS);
        dbrefAppointments.push().setValue(mapAppointmentDetails, new DatabaseReference.CompletionListener() {

            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                if (databaseError != null) {
                    Log.e("BookAppointment", "codeToBookAppointmentOnFirebase() - onComplete() - databaseError: " + databaseError);

                    pdgProgressDialog.dismiss();

                    Snackbar.make(tiedCurrentCondition, "Appointment not Booked Successfully. Please try again, later.", Snackbar.LENGTH_LONG).show();

                } else {

                    String sAppointmentID = databaseReference.getKey();

                    codeToSaveAppointmentIndexOnFirebase(sAppointmentID, sPreferredDate, sPreferredTime);

                    tiedPatientName.setText("");
                    tiedContactNumber.setText("");
                    tiedCurrentCondition.setText("");

                    Snackbar.make(tiedCurrentCondition, "Appointment Booked Successfully.", Snackbar.LENGTH_LONG).show();
                }

            }
        });

    }

    /**
     * TODO: JavaDoc.
     *
     * @param appointmentID             (String)
     * @param preferredDate             (String)
     * @param preferredTime             (String)
     */
    private void codeToSaveAppointmentIndexOnFirebase(String appointmentID, String preferredDate, String preferredTime) {

        ModelFBAppointmentIndex clsModelFBAppointmentIndex = new ModelFBAppointmentIndex();
        clsModelFBAppointmentIndex.setPreferred_date(preferredDate);
        clsModelFBAppointmentIndex.setPreferred_time(preferredTime);

        HashMap<String, Object> mapAppointmentIndex = clsModelFBAppointmentIndex.codeToGenerateAppointmentIndexHashMap();

        DatabaseReference dbrefAppointmentIndex = fbdbFirebaseDatabase.getReference(AppConstants.sDBREF_APPOINTMENT_INDEX);
        dbrefAppointmentIndex.child(appointmentID).setValue(mapAppointmentIndex, new DatabaseReference.CompletionListener() {

            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                if (databaseError != null) {
                    Log.e("BookAppointment", "codeToSaveAppointmentIndexOnFirebase() - dbrefAppointmentIndex - onComplete() - databaseError: " + databaseError);
                }

                pdgProgressDialog.dismiss();

            }

        });

    }

    /**
     * This method gets final User Input,
     * and Saves it to Database.
     *
     * Called in this.clkBookAppointment.onClick();
     */
    private void codeToSubmitUserDataAndBookAppointment() {

        String sPatientName = tiedPatientName.getText().toString();
        String sContactNumber = tiedContactNumber.getText().toString();
        String sHospitalName = spnHospitalName.getSelectedItem().toString();
        String sDate = this.sPreferredDate;
        String sTime = this.sPreferredTime;
        String sSpecialist = spnSpecialist.getSelectedItem().toString();
        String sCurrentCondition = tiedCurrentCondition.getText().toString();

        new DBWorker(BookAppointmentActivity.this).execute(
                AppConstants.sEXECUTETAG_BOOK_APPOINTMENT,
                sPatientName,
                sContactNumber,
                sHospitalName,
                sDate,
                sTime,
                sSpecialist,
                sCurrentCondition);











        Bundle bunAppointmentDetails = new Bundle();
        bunAppointmentDetails.putString(AppConstants.sCOL_PATIENT_NAME, sPatientName);
        bunAppointmentDetails.putString(AppConstants.sCOL_CONTACT_NUMBER, sContactNumber);
        bunAppointmentDetails.putString(AppConstants.sCOL_HOSPITAL_NAME, sHospitalName);
        bunAppointmentDetails.putString(AppConstants.sCOL_PREFERRED_DATE, sDate);
        bunAppointmentDetails.putString(AppConstants.sCOL_PREFERRED_TIME, sTime);
        bunAppointmentDetails.putString(AppConstants.sCOL_SPECIALIST, sSpecialist);
        bunAppointmentDetails.putString(AppConstants.sCOL_CURRENT_CONDITION, sCurrentCondition);

        try {
            new DBHelper(BookAppointmentActivity.this).codeToWriteToTableAppointments(bunAppointmentDetails);

            tiedPatientName.setText("");
            tiedContactNumber.setText("");
            tiedCurrentCondition.setText("");

            Snackbar.make(tiedCurrentCondition, "Appointment Booked Successfully.", Snackbar.LENGTH_LONG).show();
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();

            Snackbar.make(tiedCurrentCondition, "Appointment not Booked Successfully. Please try again, later.", Snackbar.LENGTH_LONG).show();
        }
    }


    /**
     * View.OnClickListener interface for Views on this Activity.
     * Handles clicks on this Activity.
     * <p>
     * Called in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkBookAppointment = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnBABookAppointment:

                    if (codeToValidateUserInput()) {
                        codeToCheckCollusionOfAppointments();
                    }

                    break;
            }
        }
    };

    /**
     * DatePicker.OnDateChangedListener interface to monitor date changes,
     * on the DatePicker.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private DatePicker.OnDateChangedListener odclPreferredDate = new DatePicker.OnDateChangedListener() {

        @Override
        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            int iMonthOfYear = monthOfYear + 1;
            sPreferredDate = dayOfMonth + "/" + iMonthOfYear + "/" + year;

        }
    };

    /**
     * TimePicker.OnTimeChangedListener interface to monitor time changes,
     * on the TimePicker.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private TimePicker.OnTimeChangedListener otclPreferredTime = new TimePicker.OnTimeChangedListener() {

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

            String sTimeOfDay;

            if (hourOfDay > 12) {
                sTimeOfDay = "PM";
            } else if (hourOfDay < 12) {
                sTimeOfDay = "AM";
            } else {
                sTimeOfDay = "AM/PM";
            }

            sPreferredTime = hourOfDay + ":" + minute + " " + sTimeOfDay;

        }
    };

}
